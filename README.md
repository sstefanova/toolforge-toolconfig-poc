# Tool config and deployment PoC

## Overview

This repository contains PoC-level config and code for a component-centric YAML-based definition of Toolforge tools.

- `example.yaml` contains a definition of a sample tool consisting of several components that can be one of `webservice`, `continuous`, `one-off`, or `scheduled`. Each component's image can either be built from source with buildservice, reuse the image of another component, or use a (non-buildservice) prebuilt image.
- `example2.yaml` represents the same tool, but with some changes made to it.
- `ops.py` does the DAG resolution, determining which components need to be built and run, and in what order.
- `diff.py` detects changes to the configuration and determines which components need to be built/deleted/run as a result.

## Example usage

Assumes you have a virtual environment with the dependencies in `requirements.txt` installed.

```bash
$ python ops.py example.yaml  # defaults to giving the ops order for when a tool is deployed from scratch
Build: ['toolhunt-ui', 'toolhunt-api']
Run:
Group 1: ['migrate-db', 'toolhunt-ui', 'celery-worker', 'toolhunt-flower']
Group 2: ['update-db', 'seed-db']
Group 3: ['toolhunt-api']

$ python ops.py example.yaml --component=toolhunt-api # will output the components that need to be rebuilt/redeployed when toolhunt-api has been updated
Build: ['toolhunt-api']
Run:
Group 1: ['toolhunt-flower', 'migrate-db']
Group 2: ['seed-db', 'update-db']
Group 3: ['toolhunt-api']

$ python diff.py example.yaml example2.yaml
Delete: ['toolhunt-flower']
Builds (Parallel): ['new-job']
Run Groups:
Group 1: ['new-job']
```
