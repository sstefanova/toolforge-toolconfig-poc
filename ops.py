import yaml

import click


def load_yaml_config(filepath):
    """Load YAML configuration from the specified file."""
    with open(filepath, 'r') as f:
        return yaml.safe_load(f)


def extract_dependencies(config):
    """Extract build and run dependencies from the configuration."""
    dependencies = {}
    for comp, details in config.get('components', {}).items():
        build_info = {}

        if 'repository' in details.get('build', {}):
            build_info['repository'] = details['build']['repository']
        elif 'reuse-from' in details.get('build', {}):
            build_info['reuse-from'] = details['build']['reuse-from']
        elif 'use-prebuilt' in details.get('build', {}):
            build_info['use-prebuilt'] = details['build']['use-prebuilt']

        run_dependencies = []
        if 'wait-for' in details.get('run', {}):
            run_dependencies = details['run']['wait-for']

        dependencies[comp] = {
            'build': build_info,
            'run': run_dependencies
        }

    return dependencies


def filter_dependencies_for_rebuild(dependencies, rebuilt_component):
    """
    Filter out components unrelated to the rebuilt component and its downstream dependencies.
    """
    filtered_deps = {rebuilt_component: dependencies[rebuilt_component]}

    queue = [rebuilt_component]

	# BFS
    while queue:
        current = queue.pop(0)
        for comp, details in dependencies.items():
            # Check if this component reuses the build from 'current'
            if 'reuse-from' in details['build'] and details['build']['reuse-from'] == current:
                # Add to filtered_deps if not already included
                if comp not in filtered_deps:
                    filtered_deps[comp] = details
                    queue.append(comp)

    return filtered_deps


def generate_parallel_builds(dependencies):
    """Identify builds that can occur in parallel, considering only components that directly build from a repository."""
    parallel_builds = []
    for comp, deps in dependencies.items():
        build_info = deps.get('build', {})
        if 'repository' in build_info:
            parallel_builds.append(comp)
    return parallel_builds


def generate_run_groups(dependencies):
    """Organize runs into groups based on dependencies for sequential execution."""
    run_groups = []
    remaining = set(dependencies.keys())

    while remaining:
        group = set()
        for comp in remaining:
            run_conf = dependencies[comp].get('run', [])
            run_deps = run_conf if isinstance(run_conf, list) else [run_conf]
            if all(dep not in remaining for dep in run_deps):
                group.add(comp)

        if not group:
            raise ValueError("Circular dependency detected or unresolved dependencies.")

        run_groups.append(list(group))
        remaining -= group

    return run_groups


def process_dependencies(config_path, component):
    config = load_yaml_config(config_path)
    dependencies = extract_dependencies(config)

    if component != 'all':
        dependencies = filter_dependencies_for_rebuild(dependencies, component)

    parallel_builds = generate_parallel_builds(dependencies)
    print("Build:", parallel_builds)

    run_groups = generate_run_groups(dependencies)
    print("Run:")
    for i, group in enumerate(run_groups, start=1):
        print(f"Group {i}: {group}")


@click.command()
@click.argument('config_path', type=click.Path(exists=True))
@click.option('--component', default='all', help='Specify a component to process or "all" to process all components.')
def main(config_path, component):
    process_dependencies(config_path, component)


if __name__ == '__main__':
    main()
