import click

from ops import load_yaml_config, extract_dependencies, generate_parallel_builds, generate_run_groups


def diff_configs(original_config, updated_config):
    '''Compare two configuration dictionaries and identify which components have been added or removed.'''
    original_components = set(original_config.get('components', {}).keys())
    updated_components = set(updated_config.get('components', {}).keys())
    added_components = updated_components - original_components
    removed_components = original_components - updated_components
    return added_components, removed_components


def compute_execution_plan(config, components):
    '''Compute the build and run execution plan for a given set of components based on the configuration.'''
    dependencies = extract_dependencies(config)
    selected_dependencies = {comp: dependencies[comp] for comp in components if comp in dependencies}
    parallel_builds = generate_parallel_builds(selected_dependencies)
    run_groups = generate_run_groups(selected_dependencies)
    return parallel_builds, run_groups


@click.command()
@click.argument('original_yaml', type=click.Path(exists=True))
@click.argument('updated_yaml', type=click.Path(exists=True))
def main(original_yaml, updated_yaml):
    original_config = load_yaml_config(original_yaml)
    updated_config = load_yaml_config(updated_yaml)

    added_components, removed_components = diff_configs(original_config, updated_config)
    parallel_builds, run_groups = compute_execution_plan(updated_config, added_components)

    click.echo("Delete: {}".format(list(removed_components)))
    click.echo("Build (Parallel): {}".format(parallel_builds))
    click.echo("Run Groups:")
    for i, group in enumerate(run_groups, start=1):
        click.echo(f"Group {i}: {group}")


if __name__ == '__main__':
    main()
